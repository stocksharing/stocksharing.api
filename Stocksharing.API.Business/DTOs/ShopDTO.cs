﻿using System;

namespace Stocksharing.API.Business.DTOs
{
    /// <summary>
    /// DTO магазина
    /// </summary>
    public class ShopDTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ссылка на логотип
        /// </summary>
        public string Logo { get; set; }
    }
}
