﻿using AutoMapper;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Entities;

namespace Stocksharing.API.Business.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
                //.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.MiddleName));
        }
    }
}
