﻿using AutoMapper;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Entities;

namespace Stocksharing.API.Business.Profiles
{
    public class CouponProfile : Profile
    {
        public CouponProfile()
        {
            CreateMap<Coupon, CouponDTO>();

            CreateMap<CouponDTO, Coupon>();
        }
    }
}
