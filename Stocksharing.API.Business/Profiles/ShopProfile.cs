﻿using AutoMapper;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Entities;

namespace Stocksharing.API.Business.Profiles
{
    public class ShopProfile : Profile
    {
        public ShopProfile()
        {
            CreateMap<Shop, ShopDTO>();

            CreateMap<ShopDTO, Shop>();
        }
    }
}
