﻿using Stocksharing.API.Business.DTOs;

namespace Stocksharing.API.Business.Responses
{
    /// <summary>
    /// Результат авторизации
    /// </summary>
    public class LoginResponse
    {
        /// <summary>
        /// Авторизация прошла успешно
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public UserDTO User { get; set; }
    }
}
