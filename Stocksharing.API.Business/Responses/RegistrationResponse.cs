﻿using System;

namespace Stocksharing.API.Business.Responses
{
    /// <summary>
    /// Результат регистрации
    /// </summary>
    public class RegistrationResponse
    {
        /// <summary>
        /// Регистрация прошла успешно
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
