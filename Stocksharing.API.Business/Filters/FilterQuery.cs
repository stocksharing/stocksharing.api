﻿using Stocksharing.API.Data.Enums;

namespace Stocksharing.API.Business.Filters
{
    /// <summary>
    /// Фильтр запроса
    /// </summary>
    public class FilterQuery
    {
        /// <summary>
        /// Идентификатор магазина
        /// </summary>
        public long? ShopId { get; set; }

        /// <summary>
        /// Тип купона
        /// </summary>
        public CouponTypes Type { get; set; }

        /// <summary>
        /// Пагинация
        /// </summary>
        public Pagination Pagination { get; set; }

        /// <summary>
        /// Сортировка
        /// </summary>
        public Sorting Sorting { get; set; }
    }
}
