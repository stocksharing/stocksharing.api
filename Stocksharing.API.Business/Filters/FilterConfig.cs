﻿using System.Collections.Generic;

namespace Stocksharing.API.Business.Filters
{
    /// <summary>
    /// Конфигурация фильтра
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Список магазинов
        /// </summary>
        public KeyValuePair<long, string>[] Shops;
    }
}
