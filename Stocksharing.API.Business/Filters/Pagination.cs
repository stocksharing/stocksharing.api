﻿using System;

namespace Stocksharing.API.Business.Filters
{
    /// <summary>
    /// Пагинация
    /// </summary>
    public class Pagination
    {
        /// <summary>
        /// Номер текущей страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество объектов на странице
        /// </summary>
        public int PageSize { get; set; }
    }
}
