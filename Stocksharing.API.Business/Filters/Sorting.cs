﻿using Stocksharing.API.Data.Enums;

namespace Stocksharing.API.Business.Filters
{
    /// <summary>
    /// Сортировка
    /// </summary>
    public class Sorting
    {
        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortTypes Type { get; set; }
    }
}
