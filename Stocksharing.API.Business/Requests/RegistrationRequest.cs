﻿using System.ComponentModel.DataAnnotations;

namespace Stocksharing.API.Business.Requests
{
    /// <summary>
    /// Данные для регистрации
    /// </summary>
    public class RegistrationRequest
    {
        /// <summary>
        /// Электронная почта
        /// </summary>
        [Required(ErrorMessage = "Не указана электронная почта")]
        [EmailAddress(ErrorMessage = "Некорректная электронная почта")]
        [MaxLength(340, ErrorMessage = "Максимальная длина электронной почты {1} символов")]
        public string Email { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [Required(ErrorMessage = "Не указано имя")]
        [MaxLength(40, ErrorMessage = "Максимальная длина имени {1} символов")]
        public string FirstName { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required(ErrorMessage = "Не указан пароль")]
        [MaxLength(60, ErrorMessage = "Максимальная длина пароля {1} символов")]
        public string Password { get; set; }
    }
}
