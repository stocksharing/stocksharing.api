﻿using System.ComponentModel;

namespace Stocksharing.API.Data.Enums
{
    public enum CouponTypes
    {
        [Description("Проценты")]
        Percent = 0,

        [Description("Баллы")]
        Point = 1,

        [Description("Деньги")]
        Money = 2
    }
}
