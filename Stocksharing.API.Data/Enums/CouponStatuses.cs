﻿using System.ComponentModel;

namespace Stocksharing.API.Data.Enums
{
    public enum CouponStatuses
    {
        [Description("Новый")]
        New = 0,

        [Description("Забронирован")]
        Booked = 1,

        [Description("Закрыт")]
        Closed = 2
    }
}
