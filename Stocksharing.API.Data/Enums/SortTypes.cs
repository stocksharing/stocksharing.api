﻿using System.ComponentModel;

namespace Stocksharing.API.Data.Enums
{
    public enum SortTypes
    {
        [Description("По возрастанию")]
        Asc = 0,

        [Description("По убыванию")]
        Desc = 1
    }
}
