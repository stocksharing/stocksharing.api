﻿using Stocksharing.API.Data.Enums;
using System;
using System.Collections.Generic;

namespace Stocksharing.API.Data.Entities
{
    /// <summary>
    /// Купон
    /// </summary>
    public class Coupon
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор владелеца
        /// </summary>
        public long OwnerId { get; set; }

        /// <summary>
        /// Идентификатор магазина
        /// </summary>
        public long ShopId { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public CouponTypes Type { get; set; }

        /// <summary>
        /// Скидка
        /// </summary>
        public int Discount { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Ссылка на изображение
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public CouponStatuses Status { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Дата обновления
        /// </summary>
        public DateTime UpdatedDate { get; set; }

        /// <summary>
        /// Владелец
        /// </summary>
        public User Owner { get; set; }

        /// <summary>
        /// Магазин
        /// </summary>
        public Shop Shop { get; set; }

        /// <summary>
        /// Бронирования
        /// </summary>
        public List<Booking> Bookings { get; set; }
    }
}
