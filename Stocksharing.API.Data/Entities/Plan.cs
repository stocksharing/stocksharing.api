﻿using System.Collections.Generic;

namespace Stocksharing.API.Data.Entities
{
    /// <summary>
    /// План
    /// </summary>
    public class Plan
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Количество бронирований
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Активный
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Подписки
        /// </summary>
        public List<Subscription> Subscriptions { get; set; }
    }
}
