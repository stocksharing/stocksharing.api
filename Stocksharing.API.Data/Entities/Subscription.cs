﻿using System;
using System.Collections.Generic;

namespace Stocksharing.API.Data.Entities
{
    /// <summary>
    /// Подписка
    /// </summary>
    public class Subscription
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор плана
        /// </summary>
        public long PlanId { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Активна
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// План
        /// </summary>
        public Plan Plan { get; set; }

        /// <summary>
        /// Бронирования
        /// </summary>
        public List<Booking> Bookings { get; set; }
    }
}
