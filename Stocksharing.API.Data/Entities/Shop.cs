﻿using System.Collections.Generic;

namespace Stocksharing.API.Data.Entities
{
    /// <summary>
    /// Магазин
    /// </summary>
    public class Shop
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ссылка на логотип
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// Удален
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Купоны
        /// </summary>
        public List<Coupon> Coupons { get; set; }
    }
}
