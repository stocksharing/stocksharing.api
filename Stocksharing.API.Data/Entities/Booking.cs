﻿using System;

namespace Stocksharing.API.Data.Entities
{
    /// <summary>
    /// Бронирование
    /// </summary>
    public class Booking
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор купона
        /// </summary>
        public long CouponId { get; set; }

        /// <summary>
        /// Идентификатор подписки
        /// </summary>
        public long SubscriptionId { get; set; }

        /// <summary>
        /// Даба бронирования
        /// </summary>
        public DateTime BookedDate { get; set; }

        /// <summary>
        /// Секретный код
        /// </summary>
        public string SecretCode { get; set; }

        /// <summary>
        /// Купон
        /// </summary>
        public Coupon Coupon { get; set; }

        /// <summary>
        /// Подписка
        /// </summary>
        public Subscription Subscription { get; set; }
    }
}
