﻿using System;
using System.Collections.Generic;

namespace Stocksharing.API.Data.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Хэш пароля
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Дата обновления
        /// </summary>
        public DateTime UpdatedDate { get; set; }

        /// <summary>
        /// Заблокирован
        /// </summary>
        public bool IsBlocked { get; set; }

        /// <summary>
        /// Принадлежащие купоны
        /// </summary>
        public List<Coupon> Coupons { get; set; }

        /// <summary>
        /// Подписки
        /// </summary>
        public List<Subscription> Subscriptions { get; set; }
    }
}
