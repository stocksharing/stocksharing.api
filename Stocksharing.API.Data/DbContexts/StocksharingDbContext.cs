﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Stocksharing.API.Data.Entities;
using System;
using System.Reflection;

namespace Stocksharing.API.Data.Contexts
{
    public class StocksharingDbContext : DbContext
    {
        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Планы
        /// </summary>
        public DbSet<Plan> Plans { get; set; }

        /// <summary>
        /// Подписки
        /// </summary>
        public DbSet<Subscription> Subscriptions { get; set; }

        /// <summary>
        /// Бронирования
        /// </summary>
        public DbSet<Booking> Bookings { get; set; }

        /// <summary>
        /// Купоны
        /// </summary>
        public DbSet<Coupon> Coupons { get; set; }

        /// <summary>
        /// Магазины
        /// </summary>
        public DbSet<Shop> Shops { get; set; }

        public StocksharingDbContext()
        {
        }

        public StocksharingDbContext(DbContextOptions<StocksharingDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var basePath = AppContext.BaseDirectory;

            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json");

            var configuration = builder.Build();

            optionsBuilder.UseSqlServer(configuration["ConnectionStrings:Stocksharing"]);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
