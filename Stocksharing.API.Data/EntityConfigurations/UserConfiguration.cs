﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Stocksharing.API.Data.Entities;

namespace Stocksharing.API.Data.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(p => p.Email)
                .HasMaxLength(340)
                .IsRequired();

            builder.Property(p => p.PasswordHash)
                .HasMaxLength(60)
                .IsRequired();

            builder.Property(p => p.LastName)
                .HasMaxLength(40);

            builder.Property(p => p.FirstName)
                .HasMaxLength(40)
                .IsRequired();

            builder.Property(p => p.MiddleName)
                .HasMaxLength(40);

            builder.Property(p => p.FullName)
                .HasComputedColumnSql("LTRIM(RTRIM(ISNULL([LastName], '') + ' ' + [FirstName] + ' ' + ISNULL([MiddleName], '')))");

            builder.Property(p => p.Phone)
                .HasMaxLength(20);
        }
    }
}
