﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Stocksharing.API.Data.Entities;

namespace Stocksharing.API.Data.EntityConfigurations
{
    public class BookingConfiguration : IEntityTypeConfiguration<Booking>
    {
        public void Configure(EntityTypeBuilder<Booking> builder)
        {
            builder.Property(p => p.SecretCode)
                .HasMaxLength(200);

            builder
                .HasOne(p => p.Subscription)
                .WithMany(t => t.Bookings)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
