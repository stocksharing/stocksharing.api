﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Stocksharing.API.Data.Entities;

namespace Stocksharing.API.Data.EntityConfigurations
{
    public class CouponConfiguration : IEntityTypeConfiguration<Coupon>
    {
        public void Configure(EntityTypeBuilder<Coupon> builder)
        {
            builder.Property(p => p.Code)
                .HasMaxLength(200);

            builder.Property(p => p.Image)
                .HasMaxLength(200);

            builder.HasIndex(p => new { p.Type, p.Status });
        }
    }
}
