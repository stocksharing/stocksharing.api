﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Stocksharing.API.Data.Migrations
{
    public partial class UsersAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(maxLength: 340, nullable: false),
                    PasswordHash = table.Column<string>(maxLength: 60, nullable: false),
                    LastName = table.Column<string>(maxLength: 40, nullable: true),
                    FirstName = table.Column<string>(maxLength: 40, nullable: false),
                    MiddleName = table.Column<string>(maxLength: 40, nullable: true),
                    FullName = table.Column<string>(nullable: true, computedColumnSql: "LTRIM(RTRIM(ISNULL([LastName], '') + ' ' + [FirstName] + ' ' + ISNULL([MiddleName], '')))"),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsBlocked = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
