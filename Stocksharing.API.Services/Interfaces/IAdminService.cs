﻿using Stocksharing.API.Business.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    public interface IAdminService
    {
        /// <summary>
        /// Получить список пользователей
        /// </summary>
        /// <returns></returns>
        public Task<List<UserDTO>> GetUsersAsync();
    }
}
