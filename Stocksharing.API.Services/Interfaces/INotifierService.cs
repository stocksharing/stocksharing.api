﻿using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для отправки уведомлений
    /// </summary>
    public interface INotifierService
    {
        /// <summary>
        /// Отправить уведомление о регистрации
        /// </summary>
        /// <param name="email">Электронная почта</param>
        /// <returns></returns>
        public Task SendRegistrationNotificationAsync(string email);

        /// <summary>
        /// Отправить уведомление о блокировке
        /// </summary>
        /// <param name="email">Электронная почта</param>
        /// <returns></returns>
        public Task SendBlockNotificationAsync(string email);

        /// <summary>
        /// Отправить уведомление о добавлении купона
        /// </summary>
        /// <param name="email">Электронная почта</param>
        /// <returns></returns>
        public Task SendCouponAdditionNotification(string email);
    }
}
