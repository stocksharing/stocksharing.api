﻿using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Business.Filters;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для поиска купонов
    /// </summary>
    public interface ISearchService
    {
        /// <summary>
        /// Получить конфигурацию фильтра
        /// </summary>
        /// <returns></returns>
        public Task<FilterConfig> GetFilterConfigAsync();

        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="filterQuery">Фильтр запроса</param>
        /// <returns></returns>
        public Task<CouponDTO[]> SearchAsync(FilterQuery filterQuery);
    }
}
