﻿using System;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с токеном
    /// </summary>
    public interface IJWTService
    {
        /// <summary>
        /// Получить токен
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        public string Get(long id);

        /// <summary>
        /// Проверить токен на валидность
        /// </summary>
        /// <param name="token">Токен</param>
        /// <returns></returns>
        public bool Validate(string token);
    }
}
