﻿using Stocksharing.API.Business.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с пользователями
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Получить идентификатор пользователя из токена (-1, если не авторизован)
        /// </summary>
        /// <returns></returns>
        public long GetUserId();

        /// <summary>
        /// Проверить наличие ролей у пользователя
        /// </summary>
        /// <param name="roles">Массив ролей для проверки</param>
        /// <returns></returns>
        public bool IsInRoles(params string[] roles);

        /// <summary>
        /// Получить список
        /// </summary>
        /// <returns></returns>
        public Task<UserDTO[]> GetUsersAsync();

        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="search">Строка поиска</param>
        /// <param name="top">Количество записей</param>
        /// <returns></returns>
        public Task<KeyValuePair<long, string>[]> SearchUsersAsync(string search, int top);

        /// <summary>
        /// Заблокировать
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        public Task<UserDTO> BlockAsync(long id);
    }
}
