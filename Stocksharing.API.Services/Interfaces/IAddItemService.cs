﻿using Stocksharing.API.Business.DTOs;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для добавления купонов
    /// </summary>
    public interface IAddItemService
    {
        /// <summary>
        /// Добавить купон
        /// </summary>
        /// <param name="couponDTO">Добавляемый купон</param>
        /// <returns></returns>
        public Task AddAsync(CouponDTO couponDTO);
    }
}
