﻿using Stocksharing.API.Business.Requests;
using Stocksharing.API.Business.Responses;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для доступа в приложение
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Вход
        /// </summary>
        /// <param name="request">Данные для входа</param>
        /// <returns></returns>
        public Task<LoginResponse> LoginAsync(LoginRequest request);

        /// <summary>
        /// Выход
        /// </summary>
        /// <returns></returns>
        public void Logout();

        /// <summary>
        /// Регистрация
        /// </summary>
        /// <param name="request">Данные для регистрации</param>
        /// <returns></returns>
        public Task<RegistrationResponse> RegistrationAsync(RegistrationRequest request);
    }
}
