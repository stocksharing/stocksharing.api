﻿using Stocksharing.API.Business.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с магазинами
    /// </summary>
    public interface IShopService
    {
        /// <summary>
        /// Получить список
        /// </summary>
        /// <returns></returns>
        public Task<ShopDTO[]> GetShopsAsync();

        /// <summary>
        /// Добавить
        /// </summary>
        /// <param name="shopDTO">Добавляемый магазин</param>
        /// <returns></returns>
        public Task<ShopDTO> AddAsync(ShopDTO shopDTO);

        /// <summary>
        /// Обновить
        /// </summary>
        /// <param name="shopDTO">Обновляемый магазин</param>
        /// <returns></returns>
        public Task<ShopDTO> UpdateAsync(ShopDTO shopDTO);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">Идентификатор магазина</param>
        /// <returns></returns>
        public Task DeleteAsync(long id);
    }
}
