﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Data.Entities;
using Stocksharing.API.Data.Enums;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Services
{
    public class AddItemService : IAddItemService
    {
        private readonly StocksharingDbContext _stocksharingDbContext;
        private readonly INotifierService _notifierService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AddItemService(StocksharingDbContext stocksharingDbContext, INotifierService notifierService, IUserService userService, IMapper mapper)
        {
            _stocksharingDbContext = stocksharingDbContext;
            _notifierService = notifierService;
            _userService = userService;
            _mapper = mapper;
        }

        public async Task AddAsync(CouponDTO couponDTO)
        {
            var userId = _userService.GetUserId();

            var user = await _stocksharingDbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

            var coupon = _mapper.Map<Coupon>(couponDTO);

            var createdDate = DateTime.Now;

            coupon.Owner = user;
            coupon.Status = CouponStatuses.New;
            coupon.CreatedDate = createdDate;
            coupon.UpdatedDate = createdDate;

            await _stocksharingDbContext.Coupons.AddAsync(coupon);
            await _stocksharingDbContext.SaveChangesAsync();

            await _notifierService.SendCouponAdditionNotification(user.Email);
        }
    }
}
