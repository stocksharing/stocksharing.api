﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Business.Requests;
using Stocksharing.API.Business.Responses;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Data.Entities;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Threading.Tasks;
using static BCrypt.Net.BCrypt;

namespace Stocksharing.API.Services.Services
{
    public class AccountService : IAccountService
    {
        private readonly StocksharingDbContext _stocksharingDbContext;
        private readonly INotifierService _notifierService;
        private readonly HttpContext _httpContext;
        private readonly IJWTService _jwtService;
        private readonly IMapper _mapper;

        private readonly string _jwtCookieName; // Название куки JWT
        private readonly int _expires; // Время жизни в минутах

        public AccountService(StocksharingDbContext stocksharingDbContext,
            INotifierService notifierService,
            IHttpContextAccessor accessor,
            IJWTService jwtService,
            IMapper mapper,
            IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _stocksharingDbContext = stocksharingDbContext;
            _notifierService = notifierService;
            _httpContext = accessor.HttpContext;
            _jwtService = jwtService;
            _mapper = mapper;
            _jwtCookieName = configuration["Cookies:JWT"];
            _expires = int.Parse(configuration["Authentication:Expires"]);
        }

        public async Task<LoginResponse> LoginAsync(LoginRequest request)
        {
            var result = new LoginResponse();

            var user = await _stocksharingDbContext.Users.FirstOrDefaultAsync(x => x.Email.Equals(request.Email));
            if (user == null)
            {
                result.Success = false;
                result.ErrorMessage = "Пользователь не найден";

                return result;
            }

            var isAuthentication = EnhancedVerify(request.Password, user.PasswordHash);
            if (!isAuthentication)
            {
                result.Success = false;
                result.ErrorMessage = "Неправильный логин или пароль";

                return result;
            }

            if (user.IsBlocked)
            {
                result.Success = false;
                result.ErrorMessage = "Пользователь заблокирован";

                return result;
            }

            var token = _jwtService.Get(user.Id);

            _httpContext.Response.Cookies.Append(_jwtCookieName, token, new CookieOptions
            {
                MaxAge = TimeSpan.FromMinutes(_expires)
            });

            result.Success = true;
            result.User = _mapper.Map<UserDTO>(user);

            return result;
        }

        public void Logout()
        {
            _httpContext.Response.Cookies.Delete(_jwtCookieName);
        }

        public async Task<RegistrationResponse> RegistrationAsync(RegistrationRequest request)
        {
            var result = new RegistrationResponse();

            var isExists = await _stocksharingDbContext.Users.AnyAsync(x => x.Email.Equals(request.Email));
            if (isExists)
            {
                result.Success = false;
                result.ErrorMessage = "Пользователь с такой электронной почтой уже существует";

                return result;
            }

            var createdDate = DateTime.Now;

            var user = new User
            {
                Email = request.Email,
                PasswordHash = EnhancedHashPassword(request.Password),
                FirstName = request.FirstName,
                CreatedDate = createdDate,
                UpdatedDate = createdDate
            };

            await _stocksharingDbContext.Users.AddAsync(user);

            await _stocksharingDbContext.SaveChangesAsync();

            result.Success = true;

            await _notifierService.SendRegistrationNotificationAsync(user.Email);

            return result;
        }
    }
}
