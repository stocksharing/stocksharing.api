﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Services
{
    public class UserService : IUserService
    {
        private readonly StocksharingDbContext _stocksharingDbContext;
        private readonly INotifierService _notifierService;
        private readonly HttpContext _httpContext;
        private readonly IMapper _mapper;

        public UserService(StocksharingDbContext stocksharingDbContext, INotifierService notifierService, IHttpContextAccessor accessor, IMapper mapper)
        {
            _stocksharingDbContext = stocksharingDbContext;
            _notifierService = notifierService;
            _httpContext = accessor.HttpContext;
            _mapper = mapper;
        }

        public long GetUserId()
        {
            if (long.TryParse(_httpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value, out var userId))
            {
                return userId;
            }
            return -1;
        }

        public bool IsInRoles(params string[] roles)
        {
            foreach (var role in roles)
            {
                if (_httpContext.User?.IsInRole(role) == true)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<UserDTO[]> GetUsersAsync()
        {
            var users = await _stocksharingDbContext.Users.AsNoTracking().ToArrayAsync();
            var result = _mapper.Map<UserDTO[]>(users);
            return result;
        }

        public async Task<KeyValuePair<long, string>[]> SearchUsersAsync(string search, int top)
        {
            var result = await _stocksharingDbContext.Users.Where(x => x.FullName.Contains(search))
                .Take(top)
                .Select(x => new KeyValuePair<long, string>(x.Id, x.FullName))
                .ToArrayAsync();
            return result;
        }

        public async Task<UserDTO> BlockAsync(long id)
        {
            var user = await _stocksharingDbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
            {
                throw new ArgumentException(nameof(user), "Пользователь не найден");
            }

            user.IsBlocked = true;

            await _stocksharingDbContext.SaveChangesAsync();

            await _notifierService.SendBlockNotificationAsync(user.Email);

            var result = _mapper.Map<UserDTO>(user);
            return result;
        }
    }
}
