﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Data.Entities;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Services
{
    public class ShopService : IShopService
    {
        private readonly StocksharingDbContext _stocksharingDbContext;
        private readonly IMapper _mapper;

        public ShopService(StocksharingDbContext stocksharingDbContext, IMapper mapper)
        {
            _stocksharingDbContext = stocksharingDbContext;
            _mapper = mapper;
        }

        public async Task<ShopDTO[]> GetShopsAsync()
        {
            var shops = await _stocksharingDbContext.Shops.AsNoTracking()
                .Where(x => !x.IsDeleted)
                .ToArrayAsync();

            var result = _mapper.Map<ShopDTO[]>(shops);
            return result;
        }

        public async Task<ShopDTO> AddAsync(ShopDTO shopDTO)
        {
            var isExists = await _stocksharingDbContext.Shops.AnyAsync(x => x.Name.Equals(shopDTO.Name));
            if (isExists)
            {
                throw new Exception("Такой магазин уже существует");
            }

            var shop = _mapper.Map<Shop>(shopDTO);

            await _stocksharingDbContext.Shops.AddAsync(shop);
            await _stocksharingDbContext.SaveChangesAsync();

            var result = _mapper.Map<ShopDTO>(shop);
            return result;
        }

        public async Task<ShopDTO> UpdateAsync(ShopDTO shopDTO)
        {
            var shop = await _stocksharingDbContext.Shops.FirstOrDefaultAsync(x => x.Id == shopDTO.Id);
            if (shop == null)
            {
                throw new ArgumentException(nameof(shop), "Магазин не найден");
            }

            shop = _mapper.Map<Shop>(shopDTO);

            await _stocksharingDbContext.SaveChangesAsync();

            var result = _mapper.Map<ShopDTO>(shop);
            return result;
        }

        public async Task DeleteAsync(long id)
        {
            var shop = await _stocksharingDbContext.Shops.FirstOrDefaultAsync(x => x.Id == id);
            if (shop == null)
            {
                throw new ArgumentException(nameof(shop), "Магазин не найден");
            }

            shop.IsDeleted = true;

            await _stocksharingDbContext.SaveChangesAsync();
        }
    }
}
