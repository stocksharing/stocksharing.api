﻿using Stocksharing.API.Services.Interfaces;
using Stocksharing.Common.Interfaces;
using Stocksharing.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Services
{
    public class NotifierService : INotifierService
    {
        private readonly INotifierHandler _notifierHandler;

        public NotifierService(INotifierHandler notifierHandler)
        {
            _notifierHandler = notifierHandler;
        }

        public async Task SendRegistrationNotificationAsync(string email)
        {
            var message = new Email
            {
                To = new List<string> { email },
                From = "stocksharing@stocksharing.ru",
                Subject = "Регистрация",
                Body = "Добро пожаловать на сайт"
            };

            await _notifierHandler.SendEmailAsync(message);
        }

        public async Task SendBlockNotificationAsync(string email)
        {
            var message = new Email
            {
                To = new List<string> { email },
                From = "stocksharing@stocksharing.ru",
                Subject = "Аккаунт заблокирован",
                Body = "Ваш аккаунт заблокирован"
            };

            await _notifierHandler.SendEmailAsync(message);
        }

        public async Task SendCouponAdditionNotification(string email)
        {
            var message = new Email
            {
                To = new List<string> { email },
                From = "stocksharing@stocksharing.ru",
                Subject = "Добавлен купон",
                Body = "Вы добавили купон"
            };

            await _notifierHandler.SendEmailAsync(message);
        }
    }
}
