﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Services
{
    public class AdminService : IAdminService
    {
        private readonly StocksharingDbContext _stocksharingDbContext;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public AdminService(StocksharingDbContext stocksharingDbContext, IMapper mapper, IUserService userService)
        {
            _stocksharingDbContext = stocksharingDbContext;
            _mapper = mapper;
            _userService = userService;
        }

        public async Task<List<UserDTO>> GetUsersAsync()
        {
            var users = await _stocksharingDbContext.Users.AsNoTracking().ToArrayAsync();
            var result = _mapper.Map<List<UserDTO>>(users);
            return result;
        }
    }
}
