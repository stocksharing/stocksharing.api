﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Stocksharing.API.Services.Services
{
    public class JWTService : IJWTService
    {
        private readonly string _issuer; // Издатель
        private readonly string _audience; // Потребитель
        private readonly int _expires; // Время жизни в минутах
        private readonly string _key; // Ключ для шифрования

        public JWTService(IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            var section = configuration.GetSection("Authentication");

            _issuer = section["Issuer"];
            _audience = section["Audience"];
            _expires = int.Parse(section["Expires"]);
            _key = section["Key"];
        }

        public string Get(long id)
        {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, id.ToString())
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _issuer,
                Audience = _audience,
                Expires = DateTime.UtcNow.AddMinutes(_expires),
                SigningCredentials = new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256),
                Subject = new ClaimsIdentity(claims)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);

            var token = tokenHandler.WriteToken(securityToken);

            return token;
        }

        public bool Validate(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _issuer,
                ValidateAudience = true,
                ValidAudience = _audience,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                LifetimeValidator = LifetimeValidator,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSymmetricSecurityKey()
            };

            try
            {
                var claimsPrincipal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var validatedToken);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private bool LifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if (!expires.HasValue)
                return false;

            return expires > DateTime.UtcNow;
        }

        /// <summary>
        /// Получить симметричный ключ для шифрования
        /// </summary>
        /// <returns></returns>
        private SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_key));
        }
    }
}
