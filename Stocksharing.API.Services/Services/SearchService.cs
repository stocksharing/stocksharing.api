﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Business.Filters;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Data.Enums;
using Stocksharing.API.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stocksharing.API.Services.Services
{
    public class SearchService : ISearchService
    {
        private readonly StocksharingDbContext _stocksharingDbContext;
        private readonly IShopService _shopService;
        private readonly IMapper _mapper;

        public SearchService(StocksharingDbContext stocksharingDbContext, IShopService shopService, IMapper mapper)
        {
            _stocksharingDbContext = stocksharingDbContext;
            _shopService = shopService;
            _mapper = mapper;
        }

        public async Task<FilterConfig> GetFilterConfigAsync()
        {
            var shops = await _shopService.GetShopsAsync();

            var result = new FilterConfig
            {
                Shops = shops.Select(x => new KeyValuePair<long, string>(x.Id, x.Name)).ToArray()
            };

            return result;
        }

        public async Task<CouponDTO[]> SearchAsync(FilterQuery filterQuery)
        {
            var skipCount = (filterQuery.Pagination.PageNumber - 1) * filterQuery.Pagination.PageSize;
            var takeCount = filterQuery.Pagination.PageSize;

            var query = _stocksharingDbContext.Coupons.AsNoTracking()
                .Include(x => x.Owner)
                .Where(x => x.Status == CouponStatuses.New)
                .Skip(skipCount)
                .Take(takeCount);

            if (filterQuery.ShopId.HasValue)
            {
                query = query.Where(x => x.ShopId == filterQuery.ShopId);
            }

            if (filterQuery.Sorting.Type == SortTypes.Asc)
            {
                query = query.OrderBy(x => x.Discount);
            }
            else
            {
                query = query.OrderByDescending(x => x.Discount);
            }

            var coupons = await query.ToArrayAsync();

            var result = _mapper.Map<CouponDTO[]>(coupons);

            return result;
        }
    }
}
