﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace Stocksharing.API.Middlewares
{
    public class JWTMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly string _jwtCookieName; // Название куки JWT

        public JWTMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _next = next;
            _jwtCookieName = configuration["Cookies:JWT"];
        }
        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Cookies[_jwtCookieName];

            if (!string.IsNullOrEmpty(token))
            {
                context.Request.Headers.Add("Authorization", "Bearer " + token);
            }

            context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
            context.Response.Headers.Add("X-Xss-Protection", "1");
            context.Response.Headers.Add("X-Frame-Options", "DENY");

            await _next(context);
        }
    }
}
