﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stocksharing.API.Business.Requests;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Stocksharing.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Вход
        /// </summary>
        /// <param name="request">Данные для авторизации</param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            try
            {
                var result = await _accountService.LoginAsync(request);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Выход
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("logout")]
        public IActionResult Logout()
        {
            try
            {
                _accountService.Logout();
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Регистрация
        /// </summary>
        /// <param name="request">Данные для регистрации</param>
        /// <returns></returns>
        [HttpPost("registration")]
        public async Task<IActionResult> Registration(RegistrationRequest request)
        {
            try
            {
                var result = await _accountService.RegistrationAsync(request);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}