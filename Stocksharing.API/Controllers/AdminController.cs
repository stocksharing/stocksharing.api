﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Stocksharing.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private readonly IShopService _shopService;
        private readonly IUserService _userService;

        public AdminController(IShopService shopService, IUserService userService)
        {
            _shopService = shopService;
            _userService = userService;
        }

        /// <summary>
        /// Получить список пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet("getUsers")]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var result = await _userService.GetUsersAsync();
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Поиск пользователей
        /// </summary>
        /// <param name="search">Строка поиска</param>
        /// <param name="top">Количество записей</param>
        /// <returns></returns>
        [HttpGet("searchUsers")]
        public async Task<IActionResult> SearchUsers(string search, int top)
        {
            try
            {
                var result = await _userService.SearchUsersAsync(search, top);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Заблокировать пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        [HttpGet("blockUser")]
        public async Task<IActionResult> BlockUser(long id)
        {
            try
            {
                var result = await _userService.BlockAsync(id);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Получить список магазинов
        /// </summary>
        /// <returns></returns>
        [HttpGet("getShops")]
        public async Task<IActionResult> GetShops()
        {
            try
            {
                var result = await _shopService.GetShopsAsync();
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Добавить магазин
        /// </summary>
        /// <param name="shopDTO">Добавляемый магазин</param>
        /// <returns></returns>
        [HttpPost("addShop")]
        public async Task<IActionResult> AddShop(ShopDTO shopDTO)
        {
            try
            {
                var result = await _shopService.AddAsync(shopDTO);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Обновить магазин
        /// </summary>
        /// <param name="shopDTO">Обновляемый магазин</param>
        /// <returns></returns>
        [HttpPut("updateShop")]
        public async Task<IActionResult> UpdateShop(ShopDTO shopDTO)
        {
            try
            {
                var result = await _shopService.UpdateAsync(shopDTO);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Удалить магазин
        /// </summary>
        /// <param name="id">Идентификатор магазина</param>
        /// <returns></returns>
        [HttpDelete("deleteShop")]
        public async Task<IActionResult> DeleteShop(long id)
        {
            try
            {
                await _shopService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}