﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stocksharing.API.Business.DTOs;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Stocksharing.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AddItemController : ControllerBase
    {
        private readonly IAddItemService _addItemService;

        public AddItemController(IAddItemService addItemService)
        {
            _addItemService = addItemService;
        }

        /// <summary>
        /// Добавить купон
        /// </summary>
        /// <param name="couponDTO">Добавляемый купон</param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<IActionResult> Add(CouponDTO couponDTO)
        {
            try
            {
                await _addItemService.AddAsync(couponDTO);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}