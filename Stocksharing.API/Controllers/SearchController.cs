﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stocksharing.API.Business.Filters;
using Stocksharing.API.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Stocksharing.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        /// <summary>
        /// Получить конфигурацию фильтра
        /// </summary>
        /// <returns></returns>
        [HttpGet("getFilterConfig")]
        public async Task<IActionResult> GetFilterConfig()
        {
            try
            {
                var result = await _searchService.GetFilterConfigAsync();
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="filterQuery">Фильтр запроса</param>
        /// <returns></returns>
        [HttpPost("search")]
        public async Task<IActionResult> Search(FilterQuery filterQuery)
        {
            try
            {
                var result = await _searchService.SearchAsync(filterQuery);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}