﻿using Microsoft.AspNetCore.Builder;
using Stocksharing.API.Middlewares;

namespace Stocksharing.API.Extensions
{
    public static class JWTMiddlewareExtension
    {
        public static IApplicationBuilder UseJWT(this IApplicationBuilder builder) => builder.UseMiddleware<JWTMiddleware>();
    }
}
