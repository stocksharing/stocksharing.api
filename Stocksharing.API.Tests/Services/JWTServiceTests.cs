﻿using Microsoft.Extensions.Configuration;
using Moq;
using Stocksharing.API.Services.Services;
using System.Threading;
using Xunit;

namespace Stocksharing.API.Tests.Services
{
    public class JWTServiceTests
    {
        private readonly Mock<IConfigurationSection> configurationSectionMock = new Mock<IConfigurationSection>();

        private readonly Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();

        public JWTServiceTests()
        {
            configurationSectionMock.Setup(e => e["Issuer"]).Returns("Issuer");
            configurationSectionMock.Setup(e => e["Audience"]).Returns("Audience");
            configurationSectionMock.Setup(e => e["Expires"]).Returns("1");
            configurationSectionMock.Setup(e => e["Key"]).Returns("O{emW9C}05P0sX09C216rXRa8NXQG%$M5dHEC2a2O947zyt%UPo2Hy8Hk{m*MzN5");

            configurationMock.Setup(e => e.GetSection("Authentication")).Returns(configurationSectionMock.Object);
        }

        [Fact]
        public void GetTokenNotNull()
        {
            // Arrange
            var tokenService = new JWTService(configurationMock.Object);

            // Act
            var result = tokenService.Get(0);

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void ValidateTokenWhenLifetimeIsCorrect()
        {
            // Arrange
            var tokenService = new JWTService(configurationMock.Object);

            // Act
            var token = tokenService.Get(0);
            var result = tokenService.Validate(token);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void ValidateTokenWhenLifetimeIsNotCorrect()
        {
            // Arrange
            var tokenService = new JWTService(configurationMock.Object);

            // Act
            var token = tokenService.Get(0);

            Thread.Sleep(70000);

            var result = tokenService.Validate(token);

            // Assert
            Assert.False(result);
        }
    }
}
