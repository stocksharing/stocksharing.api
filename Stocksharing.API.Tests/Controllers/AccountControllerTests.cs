using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using Stocksharing.API.Data.Entities;
using Stocksharing.API.Data.Contexts;
using Stocksharing.API.Services.Interfaces;
using Stocksharing.API.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Stocksharing.API.Tests.Controllers
{
    public class AccountControllerTests
    {
        [Fact]
        public void Test()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Email = "mister.ogienko@gmail.com",
                    PasswordHash = "$2a$11$YMiVJi5Kr0eecnlbG7sTs.SNyHG/picxKw9A4HvEzHT3VpIWXkDYG",
                    FirstName = "�������",
                    FullName = "�������",
                    CreatedDate = new DateTime(2020, 4, 25),
                    UpdatedDate = new DateTime(2020, 4, 25),
                    IsBlocked = false
                },
                new User
                {
                    Id = 2,
                    Email = "test@test.com",
                    PasswordHash = "$2a$11$yXWw4WGu2Cqm0yLGEshZ3Otj.ZSE/lF3Tx14nJC4xwzuNgml1Fr/G",
                    FirstName = "����",
                    FullName = "����",
                    CreatedDate = new DateTime(2020, 4, 26),
                    UpdatedDate = new DateTime(2020, 4, 26),
                    IsBlocked = false
                }
            };

            // var mock = new Mock<DbSet<User>>();

            // var mock = users.AsQueryable().BuildMockDbSet();
            // var enumerable = new TestAsyncEnumerable<TEntity>(data);


            // var stocksharingDbContextMock = new Mock<StocksharingDbContext>();
            // //stocksharingDbContextMock.Setup(e => e.Users).Returns(usersMock.Object);

            // var mock = new Mock<users.AsQueryable()>;

            // stocksharingDbContextMock.Setup(x => x.Users).Returns(mock.Object);

            // var options = new DbContextOptionsBuilder<StocksharingDbContext>()
            ////.UseInMemoryDatabase(databaseName: "MovieListDatabase")
            //.Options;

            // var context = new StocksharingDbContext(options);
            // context.Users.AddRange(users);
            //context.SaveChanges();

            //var usersMock = new Mock<DbSet<User>>();

            //usersMock.Setup(e => e).Returns();

            ////usersMock.Setup(e => e.Add(users[0])).Returns((User u) => u);

            ////var stocksharingDbContextMock = new Mock<StocksharingDbContext>();
            ////stocksharingDbContextMock.Setup(e => e.Users).Returns(usersMock.Object);

            //var tokenServiceMock = new Mock<ITokenService>();
            //var mapperMock = new Mock<IMapper>();

            //// Arrange
            //var accountService = new AccountService(context, tokenServiceMock.Object, mapperMock.Object);

            //var loginRequest = new LoginRequest
            //{
            //    Email = "mister.ogienko@gmail.com",
            //    Password = "123456"
            //};

            //// Act
            //var result = await accountService.LoginAsync(loginRequest);

            //// Assert
            //Assert.NotNull(result);
        }
    }
}
